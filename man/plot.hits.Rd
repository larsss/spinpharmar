\name{plot.hits}
\alias{plot.hits}
\title{ Plot spINPHARMA results }
\description{
  Plot filtered spINPHARMA results
}
\usage{
plot.hits(qual, hits, xlim=NULL, ylim=NULL, mtext="", plot.y="R",
plot.x="max.rmsd", highlights=c("background", "good", "top"),
xlab="Max RMSD(Å)", ylab="Inpharma score (R)", cex.mtext=1, ... )
}
\arguments{
  \item{qual}{ spINPHARMA qual data as obtained from \code{read.qual}. }
  \item{hits}{ indices, as obtained with \code{filter.hits}. }
}
\details{
  This function provides easy functionality to plot filtered INPHARMA results.

  See examples for more details.
}
\value{
  Returns a list with the following components:
  \item{good.inds}{ a numeric vector containing the indices of the
    filtered hits (sorted by R). }
  \item{tophits.r}{ a numeric vector containing the indices of the top
    hits filtered and sorted by R. }
  \item{inds.sf}{ a numeric vector containing the indices . }
  \item{inds.r}{ a numeric vector containing the indices . }
  \item{inds.qf}{ a numeric vector containing the indices . }
}
\references{
   Skjaerven, L. et al. (2013) \emph{Cool journal} \bold{12}, 345--356.
}
\author{ Lars Skjærven }
\seealso{ \code{\link{read.qual}}, \code{\link{plot.hits}} }
\examples{
     \dontrun{

     ## Read raw data
     data <- read.qual("out/qual.dat")

     ## Calculate RMSD
     rmsd.a <- calc.rmsd("modes_LL1.nam.all", "../../02_structures/3DNE_LL1.pdb", 299)
     rmsd.b <- calc.rmsd("modes_M77.nam.all", "../../02_structures/3DNE_M77.pdb", 299)

     ## Join INPHARMA and RMSD data
     results <- join.rmsd(data, rmsd.a, rmsd.b)

     ## Filter based on INPHARMA Score
     hits <- filter.hits(results, num.best.r=50)
     head( results[hits$good.inds,] )

    ## Plot Inpharma Score (R) vs RMSD to X-ray structures
    plot.hits(results, hits,
              highlights=c("background", "good", "top"),
              ylim=c(-0.3, 1))

     }
}
\keyword{ utilities }
