\name{read.qual}
\alias{read.qual}
\title{ Read qual data }
\description{
  Read spINPHARMA qual data.
}
\usage{
read.qual(filename)
}
\arguments{
  \item{filename}{ a single element character vector containing the name of
    the qual file to be read. }
}
\details{
  This function provides basic functionalities to parse the output from
  spINPHARMA.

  See examples for more details.
}
\value{
  Returns a matrix containing INPHARMA data with one row per docking
  combination of the two ligands. The data can be accessed through
  the column names:
  Filename of ligand 1 \dQuote{File_1} ,
  Filename of ligand 2 \dQuote{File_2} ,
  INPHARMA score (Pearson correlation coefficient) \dQuote{R} ,
  Quality factor \dQuote{Q} ,
  Scaling factor \dQuote{Sf} , and
  Slope \dQuote{Slope}.
}
\references{
   Skjaerven, L. et al. (2013) \emph{Cool journal} \bold{12}, 345--356.
}
\author{ Lars Skjærven }
\seealso{ \code{\link{read.qual}} }
\examples{
     \dontrun{

     ## Parse spINPHARMA output
     data <- read.qual("out/qual.dat")

     ## Sort on R (or INPHARMA score)
     inds <- order(data[,"R"], decreasing=T)
     head( data[inds, ] )
     }
}
\keyword{ io }
