"plot.noes" <-
  function(theo, exp, plot.labels=FALSE, 
           xlim=NULL, ylim=NULL,
           main="", sub="", 
           xlab="Theorectical NOEs",
           ylab="Experimental NOEs", ...) {

  if(plot.labels && require(maptools)) {
    pointLabel <- TRUE
  } else {
    if(plot.labels){
      pointLabel <- FALSE
    }
  }
  
  if(is.null(xlim))
    xlim <- c(0, max(theo, na.rm = T) )
  if(is.null(ylim))
    ylim <- c(0, max(exp, na.rm = T) )

  plot.new()
  plot.window(xlim=xlim, ylim=ylim, ...)

  for ( i in 1:nrow(theo) ) {
    x <- theo[i,]
    y <- exp[i,]
    points(x, y, col=seq(1, ncol(exp)), pch=16)

    if (plot.labels && !pointLabel) {
      label <- rownames(exp)[i]
      text(x,y, labels=label, cex=0.4, pos=1)
    }
  }

  ## Plot labels
  if (plot.labels && pointLabel) {
    excl <- which(is.na(t(exp)))
    y <- c( t(exp)[-excl] )
    x <- c( t(theo)[-excl] )
    labels <- paste(rep(rownames(exp), each=3), seq(1,3), sep=".")[-excl]
    pointLabel(x, y, labels=labels, cex=0.7)
  }

  ## Draw axes, and set titles
  axis(1)
  axis(2)
  box()

  title(main = main, sub = sub, xlab = xlab, ylab = ylab, ...)

  ## Regression
  excl <- which(is.na(exp))
  y <- c( exp[-excl] )
  x <- c( theo[-excl] )
  lm0 <- lm(y ~ x)
  ##abline(lm0)
  
  ## Pearson correlation coefficient
  excl <- which(is.na(exp))
  corr <- cor(cbind( c(theo[-excl]), c(exp[-excl])), ... )
  corr <- corr[1,2]
  slope <- summary(lm0)$coef[2,"Estimate"]

  out <- list("R"=corr, "Slope"=slope, "lm"=lm0)
  return(out)
}


 
